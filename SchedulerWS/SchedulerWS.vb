﻿Imports System.Timers
Imports System.IO
Imports System.Net

Public Class SchedulerWS

    Private eventId As Integer = 1

    Public Sub New()

        InitializeComponent()
        EventLog1 = New System.Diagnostics.EventLog()
        If Not System.Diagnostics.EventLog.SourceExists("SchedulerWS") Then
            System.Diagnostics.EventLog.CreateEventSource("SchedulerWS", "")
        End If
        EventLog1.Source = "SchedulerWS"
        EventLog1.Log = ""

    End Sub


    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        EventLog1.WriteEntry("In OnStart.")

        Dim Timer = New Timer()
        Timer.Interval = 5000  '5 seconds
        Timer.AutoReset = True
        Timer.Enabled = True
        AddHandler Timer.Elapsed, New ElapsedEventHandler(AddressOf OnTimer)
        Timer.Start()



    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        EventLog1.WriteEntry("In OnStop.")
    End Sub



    Public Sub OnTimer(ByVal sender As Object, ByVal args As ElapsedEventArgs)
        eventId += 1
        EventLog1.WriteEntry("Monitoring the System", EventLogEntryType.Information, eventId)

        'Dim time As DateTime = args.SignalTime
        'AddToFile("D:\SchedulerWS.TXT", args.SignalTime & "- SchedulerWS Run")
        Dim s As HttpWebRequest
        s = HttpWebRequest.Create("http://localhost/tech/startservice.asmx/HelloWorld")
        s.Method = "POST"
        s.ContentLength = 0
        Dim result As HttpWebResponse
        result = s.GetResponse


    End Sub

    Private Sub AddToFile(ByVal fileName As String, ByVal contents As String)

        'set up a filestream
        Dim fs As FileStream = New FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write)

        'set up a streamwriter for adding text
        Dim sw As StreamWriter = New StreamWriter(fs)

        Try

            sw.BaseStream.Seek(0, SeekOrigin.End)

            'add the text 
            sw.WriteLine(contents)
            'add the text to the underlying filestream

            sw.Flush()
            'close the writer

        Catch ex As Exception

        Finally
            sw.Close()
        End Try

    End Sub
End Class
